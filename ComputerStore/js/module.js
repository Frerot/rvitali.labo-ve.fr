// création de la base de données
const productsdb = (dbname, table) => {
    const db = new Dexie(dbname);
    db.version(1).stores(table);

// ouverture de la Base De Données
db.open().catch(function (e) {
    console.error("Open failed: " + e);
});

    return db;
    /*
        const db = new Dexie('myDb');
            db.version(1).stores({
            friends: `name, age`
        });
         */
};

// fonction d'insertion
const bulkcreate = (dbtable, data) => {
    let flag = empty(data);
    if (flag) {
    dbtable.bulkAdd([data]);
    console.log("data inserted successfully...!");
    } else {
    console.log("Please provide data...!");
    }
    return flag;
};

// création des éléments dynamiques
const createEle = (tagname, appendTo, fn) => {
    const element = document.createElement(tagname);
    if (appendTo) appendTo.appendChild(element);
    if (fn) fn(element);
};

// vérifier la validation de la zone de texte
const empty = object => {
    let flag = false;
    for (const value in object) {
    if (object[value] != "" && object.hasOwnProperty(value)) {
        flag = true;
    } else {
        flag = false;
    }
    }
    return flag;
};

// getData de la base de données
const getData = (dbname, fn) => {
    let index = 0;
    let obj = {};
    dbname.count(count => {
      // compter les lignes dans la table en utilisant la méthode count
    if (count) {
        dbname.each(table => {
          // table => renvoyer les données d'objet de table
          // pour organiser l'ordre que nous allons créer dans une boucle for
        obj = SortObj(table);
          fn(obj, index++); // fonction d'appel avec argument de données
        });
    } else {
        fn(0);
    }
    });
};

// trier un objet
const SortObj = (sortobj) => {
    let obj = {};
    obj = {
        id: sortobj.id,
        name: sortobj.name,
        seller: sortobj.seller,
        price: sortobj.price
    };
    return obj;
}


export default productsdb;
export {
    bulkcreate,
    createEle,
    getData,
    SortObj
};
import prodb, {
    bulkcreate,
    createEle,
    getData,
    SortObj
} from "./module.js";

// affectation du nom de la BDD et de la table
let db = prodb("Productdb", {
    products: `++id, name, seller, price`
});

// variables pour les balises d'entrée
const userid = document.getElementById("userid");
const proname = document.getElementById("proname");
const seller = document.getElementById("seller");
const price = document.getElementById("price");

// variables pour la création des boutons
const btncreate = document.getElementById("btn-create");
const btnread = document.getElementById("btn-read");
const btnupdate = document.getElementById("btn-update");
const btndelete = document.getElementById("btn-delete");
const btnexit = document.getElementById("btn-exit");

// données d'utilisateur

// écouteur d'événements pour le bouton "create"
btncreate.onclick = (event) => {
// insertion des valeurs
    let flag = bulkcreate(db.products, {
        name: proname.value,
        seller: seller.value,
        price: price.value
    });
    // réinitialiser les valeurs de la zone de texte
    //proname.value = "";
    //seller.value = "";
    // price.value = "";
    proname.value = seller.value = price.value = "";

// définition de la valeur de la zone de texte "ID"
getData(db.products, data => {
    userid.value = data.id + 1 || 1;
});
table();

// afficher le message d'insertion
let insertmsg = document.querySelector(".insertmsg");
getMsg(flag, insertmsg);
};

// écouteur d'événement pour le bouton "read"
btnread.onclick = table;

// bouton "update"
btnupdate.onclick = () => {
    const id = parseInt(userid.value || 0);
    if (id) {
// appeler la méthode de Dexie pour la mise à jour
    db.products.update(id, {
        name: proname.value,
        seller: seller.value,
        price: price.value
}).then((updated) => {
    // let get = mise à jour ? `données mises à jour` : `impossible de mettre les données à jour`;
    let get = updated ? true : false;

    // afficher le message de mise à jour
        let updatemsg = document.querySelector(".updatemsg");
        getMsg(get, updatemsg);

        proname.value = seller.value = price.value = "";
        /*console.log(get);*/
    })
    } else {
    console.log(`Please Select id: ${id}`);
    }
}

// bouton "delete" all
btndelete.onclick = () => {
    db.delete();
    db = prodb("Productdb", {
    products: `++id, name, seller, price`
    });
    db.open();
    table();
    textID(userid);
    // afficher le message d'effacement
    let deletemsg = document.querySelector(".deletemsg");
    getMsg(true, deletemsg);
}

    window.onload = event => {
// définir la valeur de la zone de texte ID
textID(event.userid);
};

// bouton "exit"
btnexit.onclick = () => {
    db.close();
}

// création d'un tableau dynamique
function table() {
    const tbody = document.getElementById("tbody");
    const notfound = document.getElementById("notfound");
    notfound.textContent = "";
    // retirer tous les enfants du Document Object Model (DOM) en premier
    while (tbody.hasChildNodes()) {
    tbody.removeChild(tbody.firstChild);
    }

    getData(db.products, (data) => {
    if (data) {
        createEle("tr", tbody, tr => {
        for (const value in data) {
            createEle("td", tr, td => {
            td.textContent = data.price === data[value] ? `€ ${data[value]}` : data[value];
            });
        }
        createEle("td", tr, td => {
            createEle("i", td, i => {
            i.className += "fas fa-edit btnedit";
            i.setAttribute(`data-id`, data.id);
            //stocker le nombre de boutons "edit"
            i.onclick = editbtn;
            });
        })
        createEle("td", tr, td => {
            createEle("i", td, i => {
            i.className += "fas fa-trash-alt btndelete";
            i.setAttribute(`data-id`, data.id);
            // stocker le nombre de boutons "delete"
            i.onclick = deletebtn;
            });
        })
        });
    } else {
        notfound.textContent = "No record found in the database...!";
    }
});
}

// icône "edit" pour éditer l'élément
const editbtn = (event) => {
    let id = parseInt(event.target.dataset.id);
    db.products.get(id, function (data) {
    let newdata = SortObj(data);
        userid.value = newdata.id || 0;
        proname.value = newdata.name || "";
        seller.value = newdata.seller || "";
        price.value = newdata.price || "";
    });
}

// icône "delete" pour enlever l'élément
const deletebtn = event => {
    let id = parseInt(event.target.dataset.id);
    db.products.delete(id);
    table();
}

// Zone de texte ID
function textID(textboxid) {
    getData(db.products, data => {
      textboxid.value = data.id + 1 || 1; // pour que la zone de texte "ID" soit égale à +1 où alors égale à 1 lors du chargement de la page
    });
}

// fonction msg
function getMsg(flag, element) {
    if (flag) {
    // appel du message "msg"
    element.className += " movedown";
    setTimeout(() => {
        element.classList.forEach(classname => {
        classname == "movedown" ? undefined : element.classList.remove('movedown');
        })
    }, 4000);
    }
}



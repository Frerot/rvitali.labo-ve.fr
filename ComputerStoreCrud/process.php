<?php

session_start();


// connection à la base de données
$mysqli = mysqli_connect('localhost', 'root', '', 'productsdb') or die(mysqli_error($mysqli));

// j'affecte la valeur par défaut aux variables
$id = 0;
$update = false;
$proname = '';
$seller = '';
$price = '';

// insertion des enregistrements de Product Name, de Seller et de Price dans la table "data" si onclick sur le bouton SAVE
if(isset($_POST['save'])) {
   $proname = $_POST['proname'];
   $seller = $_POST['seller'];
   $price = $_POST['price'];
   
   // requête
   $mysqli->query("INSERT INTO data (proname, seller, price) VALUES('$proname', '$seller', $price)") or 
           die (mysqli_error($mysqli));
   // affichage du message de sauvegarde avec la variable $SESSION en haut de la page à l'aide à l'aide des classes Bootstrap 4 appropriées 
   $_SESSION['message'] = "Record has been saved!!!";
   $_SESSION['msg_type'] = "success";
   
   // redirection de l'utilisateur vers index.php
   header("location: index.php");
}
// onclick bouton DELETE, suppression de l'enregistrement de la donnée en utilisant l'id passé de la variable $_GET['delete']
if(isset($_GET['delete'])){
    $id = $_GET['delete'];
    
    // requête vers la BDD
    $mysqli->query("DELETE FROM data WHERE id=$id") or die(mysqli_error($mysqli));
    
    // affichage du message de suppression avec la variable $SESSION en haut de la page à l'aide à l'aide des classes Bootstrap 4 appropriées
    $_SESSION['message'] = "Record has been deleted!!!";
    $_SESSION['msg_type'] = "danger";
    
    // redirection de l'utilisateur vers index.php
    header("location: index.php");
}
// onclick bouton EDIT pour sélectionner l'enregistrement existant dans la BDD
if(isset($_GET['edit'])) {
    $id = $_GET['edit'];
    $update = true;
    // requête vers la BDD
    $result = $mysqli->query("SELECT * FROM data WHERE id=$id") or  die(mysqli_error($mysqli));
    if (empty($result) == 0) {
        // on définit les variables et on les affichent dans les champs de saisie correspondants
        $row = $result->fetch_array();
        $proname = $row['proname'];
        $seller = $row['seller'];
        $price = $row['price'];
        
    }
}


// onclick bouton UPDATE pour mettre à jour les données sélectionnées à l'aide du bouton EDIT
if (isset($_POST['update'])) {
    $id = $_POST['id'];
    $proname = $_POST['proname'];
    $seller = $_POST['seller'];
    $price = $_POST['price'];
    // requête vers la BDD
    $mysqli->query("UPDATE data SET proname='$proname', seller='$seller' , price='$price' WHERE id=$id") or
            die(mysqli_error($mysqli));
    // affichage du message de mise à jour de l'enregistrement
    $_SESSION['message'] = "Record has been updated!!!";
    $_SESSION['msg_type'] = "warning";
    
    header("location: index.php");
}

// fermeture de la base de données
mysqli_close($mysqli);
<!DOCTYPE html>

<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        
        <!-- lien vers Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" crossorigin="anonymous"/>
        <!-- lien vers Font awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" crossorigin="anonymous" />
        <!-- lien vers font awesome icons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- lien vers la feuille de style -->
        <link rel="stylesheet" media="all" href="style.css">
        <title>Computer'Store</title>
    </head>
    <body>
    
        
        
    <header>
        <div class="container text-center">
            <h1 class="bg-light py-4 text-info">
                <i class="fas fa-laptop"></i> Computer'Store
            </h1>
        </div>
    </header>   
    <article>
    <!-- inclusion de process.php à partir de index.php -->
    <?php require_once 'process.php'; ?>
        
    
        <!-- création des messages de session et des types de messages pour les boutons SAVE, DELETE et UPDATE à l'aide des classes Bootstrap 4 appropriées -->
        <?php
        
        if(isset($_SESSION['message'])): ?>
        
        <div class="alert alert-<?=$_SESSION['msg_type']?>">
        
            <?php
                echo $_SESSION['message'];
                unset($_SESSION['message']);
            ?>
        </div>
        
        <?php endif ?> 
        
        <div class="container text-center">
        <?php
            // connection à la base de données
            $mysqli = mysqli_connect('localhost', 'root', '', 'productsdb') or  die(mysqli_error($mysqli));
            
            // sélection des enregistrements existants
            $result = $mysqli->query("SELECT * FROM data") or  die(mysqli_error($mysqli));
            //pre_r($result);
            //pre_r($rsult->fetch_assoc());
            ?>
        <!-- création du tableau -->
        <div class="row justify-content-center">
            <table class="table table-striped table-dark">
                <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>Seller</th>
                        <th>Price €</th>
                        <th colspan="2">action</th>
                    <tr>
                </thead>
        
        
        <div class="row justify-content-center">
        <?php
            // création de la boucle while (tant que) pour afficher les enregistrements existants au dessus du formulaire dans un tableau HTML 
            while ($row = $result->fetch_assoc()): ?>
                <tr>
                    <td><?php echo $row['proname']; ?></td>
                    <td><?php echo $row['seller']; ?></td>
                    <td><?php echo $row['price']; ?></td>
                    <!-- création du bouton de lien EDIT et DELETE, transmission de l'id de l'enregistrement en tant que variable GET dans l'url du lien -->
                    <td><a href="index.php?edit=<?php echo $row['id']; ?>"
                           class="btn btn-info">EDIT</a>
                           <a href="process.php?delete=<?php echo $row['id']; ?>"
                           class="btn btn-danger">DELETE</a></td>
                </tr>
            <?php endwhile; ?>
            </table>
        </div>
        
            <?php
            
            // fonction pour l'affichage dans le tableau
            function pre_r( $array) {
                echo '<pre>';
                print_r($array);
                echo '</pre>';
            }
          ?>
        <!-- création du formulaire avec les champs de saisie -->
        <!-- ajout des <div> et des classes Bootstrap au formulaire pour l'apparence et le centrer -->
        <div class="row justify-content-center">
            <!-- ajout de process.php à l'action du formulaire -->
            <form action="process.php" method="POST">
                <input type="hidden" name="id" value="<?php echo $id;?>">
            <div class="form-group">
            <label>Product Name</label>
            <input type="text" name="proname" class="form-control"
                   value="<?php echo $proname; ?>"placeholder="Enter the Product Name">
            </div>
            <div class="form-group">
            <label>Seller</label>
            <input type="text" name="seller" class="form-control" 
                   value="<?php echo $seller; ?>" placeholder="Enter the Seller">
            </div>
            <div class="form-group">
            <label>Price</label>
            <input type="text" name="price" class="form-control"
                   value="<?php echo $price; ?>"placeholder="Enter the Price">
            </div><br>
            <div class="row justify-content-center">
            <!-- si la condition pour la variable $update est égale à vrai alors le bouton UPDATE s'affiche -->
            <?php
            if ($update == true):
            ?>
                <button type="submit" name="update" class="btn btn-info">UPDATE</button>
            <!-- autrement c'est le bouton SAVE qui serra affiché -->
            <?php else: ?>
                <button type="submit" name="save" class="btn btn-primary">SAVE</button>
            <?php endif; ?>
                
            </div>
            </form>
        </div>
        </div>
    </article>    
    <section>
        <!-- The social media icon bar -->
        <div class="icon-bar">
            <a href="https://fr-fr.facebook.com/" class="facebook"><i class="fa fa-facebook"></i></a>
            <a href="https://twitter.com/twitter" class="twitter"><i class="fa fa-twitter"></i></a>
            <a href="https://www.google.com/" class="google"><i class="fa fa-google"></i></a>
            <a href="https://www.linkedin.com/" class="linkedin"><i class="fa fa-linkedin"></i></a>
            <a href="https://www.youtube.com/" class="youtube"><i class="fa fa-youtube"></i></a>
        </div>
    </section>
    <footer>
    <p class="cr">COPYRIGHT VITALI Renaud le 19/05/2019</p>
    </footer>    
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
